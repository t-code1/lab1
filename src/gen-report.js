const { getDataFromBitrix } = require('./get-data')
const { generateExcel } = require('./gen-excel')
const { uploadFile } = require('./api-yandex')

async function generateReport(startdate, enddate, methodFilter, methodSort) {
    console.log(startdate,enddate)
    const record = await getDataFromBitrix(startdate, enddate, methodFilter, methodSort)
    const fileName = await generateExcel(record)
    const url = await uploadFile(fileName)
    return url
}

module.exports = {
    generateReport
}