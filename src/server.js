const express = require('express');
const generateReport =  require('./gen-report.js').generateReport

const app = express()
const port = 5000
app.get('/api', async (req, res) => {
  const href = await generateReport(req.query.startDate, req.query.endDate, req.query.methodFilter, req.query.methodSort)
  console.log(href)
  res.json(JSON.stringify({href: href}))
})
app.use(express.static('../public'));

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})