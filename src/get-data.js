const { generateExcel } = require('./gen-excel')
const axios = require('axios').default
const url   = require('./config.js').tokenUser
const task = 'tasks.task.list.json?&start='
const time  = 'task.elapseditem.getlist?'
const user  = 'user.get?'

class Data {
    constructor (startDate, finishDate){
        this.track = []
        this.user = []
        this.task = []
        this.startDate = new Date(startDate)
        this.finishDate = new Date(finishDate)
        this.createdDate
    }
//  метод заполнения поля track
    setTrack(trackId, taskId, userId, trackTime){
        let indexTrack = this.track.findIndex(element => {
            return element.userId === userId && element.taskId === taskId
        })
        if(indexTrack !== -1){
            this.track[indexTrack].trackTime += parseInt(trackTime)
        }
        else{
            this.track.push({
                trackId      : trackId,
                taskId       : taskId,
                userId       : userId,
                trackTime    : parseInt(trackTime),
            })
        }
    }
//  метод заполнения поля user
    setUser(userId, userName, userLastName){
        let isExistsUser = this.user.findIndex(element => element.userId === userId) !== -1? true: false
        if(!isExistsUser){
            this.user.push({
                userId       : userId,
                userFullName : userName + ' ' + userLastName,
            })
        }
    }
//  метод заполнения поля task
    setTask(taskId, taskTitle, group, durationType, durationPlan, durationFact){
        this.task.push({
            taskId       : taskId,
            taskTitle    : taskTitle,
            groupId      : group.id !== undefined ? group.id   : 'Нет группы',
            groupName    : group.name !== undefined ? group.name : 'Нет названия',
            durationPlan : durationType === 'days' ? parseInt(durationPlan) * 24 * 3600 : durationType === 'hours'? parseInt(durationPlan) * 3600 : parseInt(durationPlan) * 60,
            durationFact : parseInt(durationFact) * 60,
        })
    }
//  сортировка треков по задачам или по сотрудникам
    sortBy(method){
        if(method){
            this.track.sort(function (x,y){
                if(x.taskId < y.taskId) return -1
                if(x.taskId > y.taskId) return 1
                return x.userId - y.userId
            })
        }
        else
            this.track.sort(function (x,y){
                if(x.userId < y.userId) return -1
                if(x.userId > y.userId) return 1
                return x.taskId - y.taskId
            })
    }
    parseTime(time){
        time = parseInt(time)
        let hours = Math.floor(time / 60 / 60)
        let minutes = Math.floor(time / 60) - (hours * 60)
        let seconds = (time % 60)
        return isNaN(time) ? 'Не установлено':hours + 'ч' + minutes + 'м' + seconds + 'с'
    }
//  метод возвращающий объект для заполнения таблицы
    getRecord(){
        let record = {
            groupName    : [],
            taskTitle    : [],  
            durationPlan : [],
            durationFact : [],
            trackTime    : [],
            userFullName : [],
            count        : 0,
        }
        this.track.forEach((trackElement) => {
            let task = this.find('task', trackElement.taskId)
            let user = this.find('user', trackElement.userId)
            
            record.groupName.push(task.groupName)
            record.taskTitle.push(task.taskTitle)
            record.durationPlan.push(this.parseTime(task.durationPlan))
            record.durationFact.push(this.parseTime(task.durationFact))
            record.trackTime.push(this.parseTime(trackElement.trackTime))
            record.userFullName.push(user.userFullName)
            record.count++
        })
        return record
    }
//  метод поиска объекта по id и полю
    find(field, id){
        switch(field){
            case 'user' :
                return this.user[this.user.findIndex(element => element.userId === id)]
            case 'task' :
                return this.task[this.task.findIndex(element => element.taskId === id)]
        }
    }
}
function filterDate(element, startDate, finishDate, type){
    switch(type){
        case 'task':
            return (Date(element.CLOSED_DATE) < Date(startDate) || Date(element.DATE_START) > Date(finishDate))? false : true
        case 'track':
            return (Date(element.CREATED_DATE) > Date(startDate) && Date(element.CREATED_DATE) < Date(finishDate))? false : true
    }
}
function parseRequest(arrayId, numPage,typeRequest){
    let requestURI = ''
    if(typeRequest === 'track'){
        requestURI = '&O[ID]=asc'
        arrayId.forEach((idElement, index) => {
            requestURI += '&F[TASK_ID][' + index + ']=' + idElement
        })   
        requestURI += '&S[0]=*&P[NAV_PARAMS][iNumPage]=' + numPage
    }
    if(typeRequest === 'user'){
        arrayId.forEach((idElement, index) => {
            requestURI += '&FILTER[ID][' + index + ']=' + idElement
        })   
    }
    return requestURI
}
async function getTasks(dataBase){
    let startRecord = 0
    let finish = false
    while(!finish){
        let taskList = await axios.get(url + task + startRecord)
        taskList.data.result.tasks.forEach(taskElement =>{
            if(filterDate(taskElement, dataBase.startDate, dataBase.finishDate, 'task'))
                dataBase.setTask(
                    taskElement.id, 
                    taskElement.title, 
                    taskElement.group, 
                    taskElement.durationType, 
                    taskElement.durationPlan, 
                    taskElement.durationFact,
                )
        })
        finish = Object.keys(taskList.data.result.tasks).length < 50 ? true : false
        startRecord += 50
    }
}
async function getTracks(dataBase, typeTrack){
    let lastPage = false
    for(let numPage = 1; !lastPage; numPage++){
        let trackList = await axios.get(url + time + parseRequest(dataBase.task.map(task => task.taskId),numPage,'track'))
        trackList.data.result.forEach(trackElement => {
            if(!typeTrack){
                if(filterDate(trackElement, dataBase.startDate, dataBase.finishDate, 'track'))
                    dataBase.setTrack(
                        trackElement.ID,
                        trackElement.TASK_ID,
                        trackElement.USER_ID,
                        parseInt(trackElement.SECONDS)
                    )
            }
            else{
                dataBase.setTrack(
                    trackElement.ID,
                    trackElement.TASK_ID,
                    trackElement.USER_ID,
                    parseInt(trackElement.SECONDS)
                )
            }   
        })
        lastPage = trackList.data.next === undefined
    }
}
async function getUsers(dataBase){
    let arrayId = dataBase.track.map(track => track.userId)
    let countUser = arrayId.length
    let countRecord = 50
    for(let numPage=0; countUser>0; countUser-=countRecord){
        countRecord = countUser<50 ? countUser : 50
        let userList = await axios.get(url + user + parseRequest(arrayId,numPage,'user'))
        userList.data.result.forEach(userElement =>{
            dataBase.setUser(userElement.ID, 
                userElement.NAME, 
                userElement.LAST_NAME)
        })
        numPage++
    }
}

async function getDataFromBitrix(startDate, finishDate, methodFilter, methodSort){
    let dataBase = new Data(startDate, finishDate)
    await getTasks(dataBase)
    await getTracks(dataBase, methodFilter)
    await getUsers(dataBase)
    dataBase.sortBy(methodSort)
    console.log(dataBase)
    let record = dataBase.getRecord()
    return record
}

module.exports = {
    getDataFromBitrix
}