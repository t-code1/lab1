const axios = require('axios').default
const fs = require('fs')
const conf = require('./config')

async function uploadFile(fileName) {
    const rootApi = encodeURI('https://cloud-api.yandex.net/v1/disk/resources')
    const fileInfo = encodeURI(rootApi + `/?path=/${fileName}`)
    const apiUpload = encodeURI(rootApi + `/upload?path=/${fileName}&overwrite=true`)
    const apiPublish = encodeURI(rootApi + `/publish?path=/${fileName}`)
    const token = `OAuth ${conf.tokenYandex}`
    
    const params = {
        headers: {
            'Authorization': token
        }
    }

    const response = await axios(apiUpload, params)
    await axios.put(response.data.href, fs.createReadStream('./' + fileName))
    await axios.put(apiPublish, null, params)
    const { data } = await axios(fileInfo, params)
    return data.public_url
}
  

module.exports = {
  uploadFile
}
