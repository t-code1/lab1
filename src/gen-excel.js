const ExcelJS = require('exceljs')
const fs = require('fs')

function parseName(date){
    const day = `0${date.getDate()}`.slice(-2)
    const month = `0${date.getMonth()+1}`.slice(-2)
    let name = 'Отчет на ' + `${day}.${month}.${date.getFullYear()}`
    return existsFile(name, 0)
}

function existsFile(name, index){
    let path = index == 0 ? `./${name}.xlsx` : `./${name}(${index}).xlsx`
    return fs.existsSync(path)? existsFile(name, ++index) : index == 0 ? name : name + '(' + index + ')'
}

async function generateExcel(record) {
    const workbook = new ExcelJS.Workbook()
    const sheet = workbook.addWorksheet('My Sheet')
    sheet.columns = [
        { header: 'Название проекта:', key: 'project', width: 32 },
        { header: 'Название задачи:', key: 'title', width: 32 },
        { header: 'План. время', key: 'timePlan', width: 16},
        { header: 'Факт. время:', key: 'timeFact', width: 16 },
        { header: 'Личное время:', key: 'time', width: 16 },
        { header: 'ФИО:', key: 'FIO', width: 32 },
        { header: 'Итого записей', key: 'total', width: 5}
    ]
    for(let index=0; index<record.groupName.length; index++)
        sheet.insertRow(2, {
            project: record.groupName[index],
            title: record.taskTitle[index], 
            timePlan: record.durationPlan[index],
            timeFact: record.durationFact[index],
            time: record.trackTime[index],
            FIO: record.userFullName[index],  
            total: record.count,
        })
    let name = parseName(new Date())
    await workbook.xlsx.writeFile(`${name}.xlsx`)
    return `${name}.xlsx`
}
module.exports = {
    generateExcel,
}